# Quickstart
Discussion forum for Nette application

## Instalation
#### Download
The best way to install `rmichalec/forum` is using Composer:
```sh
$ composer require rmichalec/forum
```
#### Registering
You can enable the extension using your neon config:
```sh
extensions:
    ForumExtension: Rmichalec\Forum\ForumExtension
```
#### Injecting
You can simply inject factory in Your Presenters/Services:
```php
public function __construct(
    Rmichalec\Forum\Message\Form\ControlFactory $messageFormFactory,
    Rmichalec\Forum\Message\Grid\ControlFactory $messageGridFactory,
    Rmichalec\Forum\Thread\Form\ControlFactory $threadFormFactory,
    Rmichalec\Forum\Thread\Grid\ControlFactory $threadGridFactory
){
    parent::__construct();
    ...
}
```
## Settings
This extension require services which implements `Rmichalec\Forum\Message\Form\FormFactoryInterface`, 
`Rmichalec\Forum\Thread\Form\FormFactoryInterface`,
`Rmichalec\Forum\Thread\ThreadManager`,
`Rmichalec\Forum\Message\MessageManager`.

## Customization
Component is fully customizable. All parameters are optional.
You can set options in config file like this:
```php
ForumExtension:
    templates:
        messageForm: '%appDir%/path/to/template.latte' 
        messageGrid: '%appDir%/path/to/template.latte' 
        threadForm: '%appDir%/path/to/template.latte' 
        threadGrid: '%appDir%/path/to/template.latte' 
    threadFormTranslation:
        inputNameLabel: 'Název vlákna'
        inputNameRequired: 'Zadejte název'
        buttonSubmitText: 'Odeslat'
    messageFormTranslation:
        inputMessageLabel: 'Zpráva'
        inputMessageRequired: 'Zadejte prosím zprávu'
        buttonSubmitText: 'Odeslat'

```
## Conclusion
This extension requires Nette 3.0 and it is property of Radomir Michalec © 2020

## Special thanks
Anotnín Jehlář 