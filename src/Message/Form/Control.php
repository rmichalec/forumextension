<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message\Form;

use Closure;
use Nette\Application\UI\Control as UiControl;
use Nette\Application\UI\Form;

class Control extends UiControl
{
    private string $templatePath;
    private FormFactory $formFactory;
    private Closure $onSuccess;
    private int $threadId;

    public function __construct(string $templatePath, FormFactory $formFactory, Closure $onSuccess, int $threadId)
    {
        $this->templatePath = $templatePath;
        $this->formFactory = $formFactory;
        $this->onSuccess = $onSuccess;
        $this->threadId = $threadId;
    }

    public function render(){
        $this->template->render($this->templatePath);
    }

    public function createComponentForm(): Form
    {
        $form = $this->formFactory->create($this->threadId);
        $form->onSuccess[] = $this->onSuccess;

        return $form;
    }
}