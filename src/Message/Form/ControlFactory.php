<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message\Form;

use Closure;

interface ControlFactory
{
    public function create(Closure $onSuccess, int $threadId): Control;
}