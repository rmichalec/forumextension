<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message\Form;

use App\Form\MessageFormFactory;
use Nette\Application\UI\Form;
use Nette\SmartObject;
use Rmichalec\Forum\Message\MessageManager;

class FormFactory
{
    use SmartObject;

    private MessageFormFactory $messageFormFactory;
    private MessageManager $messageManager;

    private string $inputMessageLabel = "";
    private string $inputMessageRequired = "";
    private string $buttonSubmitText = "";

    public function __construct(MessageFormFactory $messageFormFactory, MessageManager $messageManager)
    {
        $this->messageFormFactory = $messageFormFactory;
        $this->messageManager = $messageManager;
    }

    public function create(int $threadId): Form
    {
        $form = $this->messageFormFactory->create();

        $form->addHidden('id_thread', $threadId);

        $form->addTextArea('message', $this->inputMessageLabel)
            ->setRequired($this->inputMessageRequired);

        $form->addSubmit('send', $this->buttonSubmitText);

        $form->onSuccess[] = [$this, 'onSuccess'];

        return $form;
    }

    public function onSuccess(Form $form, array $values): void
    {
        $this->messageManager->save($values);
    }

    public function setInputMessageLabel(string $label): void
    {
        $this->inputMessageLabel = $label;
    }

    public function setInputMessageRequired(string $message): void
    {
        $this->inputMessageRequired = $message;
    }

    public function setButtonSubmitText(string $text): void
    {
        $this->buttonSubmitText = $text;
    }
}