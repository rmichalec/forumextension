<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message\Form;

use Nette\Application\UI\Form;

interface FormFactoryInterface
{
    public function create(): Form;
}