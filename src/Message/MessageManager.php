<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message;

use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use stdClass;

interface MessageManager
{
    /**
     * @param array $data [id_thread => int, message => string]
     * @return ActiveRow|stdClass
     */
    public function save(array $data);

    /**
     * @param int $id
     * @return Selection|array
     */
    public function findByThreadId(int $id);
}