<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message\Grid;

interface ControlFactory
{
    public function create(int $threadId): Control;
}