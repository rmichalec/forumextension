<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Message\Grid;

use Nette\Application\UI\Control as UiControl;
use Rmichalec\Forum\Message\MessageManager;

class Control extends UiControl
{
    private MessageManager $manager;
    private int $threadId;
    private string $templatePath;

    public function __construct(MessageManager $manager, string $templatePath, int $threadId)
    {
        $this->manager = $manager;
        $this->threadId = $threadId;
        $this->templatePath = $templatePath;
    }

    public function render(): void
    {
        $this->template->datas = $this->manager->findByThreadId($this->threadId);
        $this->template->render($this->templatePath);
    }
}