<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread;

use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use stdClass;

interface ThreadManager
{
    /**
     * @param array $data [name => string]
     * @return ActiveRow|stdClass
     */
    public function save(array $data);

    /**
     * @return Selection|array
     */
    public function findAll();
}