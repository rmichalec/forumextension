<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread\Form;

use Nette\Application\UI\Form;

interface FormFactoryInterface
{
    public function create(): Form;
}