<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread\Form;

use Closure;

interface ControlFactory
{
    public function create(Closure $onSuccess): Control;
}