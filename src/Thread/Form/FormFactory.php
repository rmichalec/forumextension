<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread\Form;

use Nette\Application\UI\Form;
use Nette\SmartObject;
use Rmichalec\Forum\Thread\ThreadManager;

class FormFactory
{
    use SmartObject;

    private ThreadManager $manager;
    private FormFactoryInterface $factory;

    private string $inputNameLabel = "";
    private string $inputNameRequired = "";
    private string $buttonSubmitText = "";

    public function __construct(ThreadManager $manager, FormFactoryInterface $factory)
    {
        $this->manager = $manager;
        $this->factory = $factory;
    }

    public function create(): Form
    {
        $form = $this->factory->create();

        $form->addText('name', $this->inputNameLabel)
            ->setRequired($this->inputNameRequired);

        $form->addSubmit('send', $this->buttonSubmitText);

        $form->onSuccess[] = [$this, 'onSuccess'];

        return $form;
    }

    public function onSuccess(Form $form, array $values): void
    {
        $this->manager->save($values);
    }

    public function setInputNameLabel(string $label): void
    {
        $this->inputNameLabel = $label;
    }

    public function setInputNameRequired(string $message): void
    {
        $this->inputNameRequired = $message;
    }

    public function setButtonSubmitText(string $text): void
    {
        $this->buttonSubmitText = $text;
    }
}