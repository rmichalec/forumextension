<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread\Form;

use Closure;
use Nette\Application\UI\Control as UiControl;
use Nette\Application\UI\Form;

class Control extends UiControl
{
    private FormFactory $formFactory;
    private Closure $onSuccess;
    private string $templatePath;

    public function __construct(FormFactory $formFactory, string $templatePath, Closure $onSuccess)
    {
        $this->formFactory = $formFactory;
        $this->onSuccess = $onSuccess;
        $this->templatePath = $templatePath;
    }

    public function render(): void
    {
        $this->template->render($this->templatePath);
    }

    public function createComponentForm(): Form
    {
        $form = $this->formFactory->create();
        $form->onSuccess[] = $this->onSuccess;

        return $form;
    }
}