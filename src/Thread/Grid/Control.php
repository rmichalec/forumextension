<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread\Grid;

use Nette\Application\UI\Control as UiControl;
use Rmichalec\Forum\Thread\ThreadManager;

class Control extends UiControl
{
    private ThreadManager $threadManager;
    private string $templatePath;

    public function __construct(ThreadManager $threadManager, string $templatePath)
    {
        $this->threadManager = $threadManager;
        $this->templatePath = $templatePath;
    }

    public function render(){
        $this->template->datas = $this->threadManager->findAll();

        $this->template->render($this->templatePath);
    }
}