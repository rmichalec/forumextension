<?php
declare(strict_types=1);

namespace Rmichalec\Forum\Thread\Grid;

interface ControlFactory
{
    public function create(): Control;
}