<?php
declare(strict_types=1);

namespace Rmichalec\Forum;

use Nette;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;

class ForumExtension extends CompilerExtension
{
    public function getConfigSchema(): Schema
    {
        return Expect::structure([
            'templates' => Expect::structure([
                'messageForm' => Expect::string(__DIR__ . '/Message/Form/default.latte'),
                'messageGrid' => Expect::string(__DIR__ . '/Message/Grid/default.latte'),
                'threadForm' => Expect::string(__DIR__ . '/Thread/Form/default.latte'),
                'threadGrid' => Expect::string(__DIR__ . '/Thread/Grid/default.latte'),
            ])->castTo('array'),
            'threadFormTranslation' => Expect::structure([
                'inputNameLabel' => Expect::string('Název vlákna'),
                'inputNameRequired' => Expect::string('Zadejte název'),
                'buttonSubmitText' => Expect::string('Odeslat'),
            ])->castTo('array'),
            'messageFormTranslation' => Expect::structure([
                'inputMessageLabel' => Expect::string('Zpráva'),
                'inputMessageRequired' => Expect::string('Zadejte prosím zprávu'),
                'buttonSubmitText' => Expect::string('Odeslat'),
            ])->castTo('array'),
        ])->castTo('array');
    }

    public function loadConfiguration()
    {
        $this->compiler->loadDefinitionsFromConfig(
            Nette\DI\Helpers::expand(
                $this->loadFromFile(__DIR__ . '/config/config.neon')['services'],
                (array) $this->config
            )
        );
    }
}